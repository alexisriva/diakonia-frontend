import Login from './components/authentication/Login.vue';
import Report from './components/reports/Report.vue';
import Order from './components/orders/Order.vue';
import OrderDetail from './components/orders/OrderDetail.vue';
import User from './components/users/User.vue';
import UserDetail from './components/users/UserDetail.vue';

export const routes = [
    { path: '', component: Report, name: 'home', beforeEnter(to, from, next) {
        next({ name: 'orders' })
    } },
    { path: '/orders', component: Order, name: 'orders' },
    { path: '/orders/:id', component: OrderDetail, name: 'orderDetail' },
    { path: '/users', component: User, name: 'users', beforeEnter(to, from, next) {
        if (localStorage.getItem('UserRole') == 0) {
            next();
        } else {
            next({ name: 'orders' });
        }
    } },
    { path: '/users/:id', component: UserDetail, name: 'userDetail' },
    { path: '/login', component: Login, name: 'login' },
];