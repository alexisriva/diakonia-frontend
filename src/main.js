import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import VModal from 'vue-js-modal'
import App from './App.vue'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

import { routes } from './routes';


Vue.use(Buefy);
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VModal);


const router = new VueRouter({
    routes,
    mode: 'history'
});

router.beforeEach((to, from, next) => {
    const token = localStorage.getItem('AuthToken');
    const authenticated = token != null && token != '' && token != 'null';
    if (to.name == 'login') {
        if (authenticated) {
            next({ name: 'orders' });
            console.log("ya estás logueado we");
        } else next();
    } else if (authenticated) {
        next();
    } else next({ name: 'login' });
});

// Vue.http.options.root = 'http://localhost:8000/'; // dev

export const eventBus = new Vue();

new Vue({
    el: '#app',
    router,
    render: h => h(App)
});